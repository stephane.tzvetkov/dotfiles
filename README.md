# My dotfiles with [chezmoi](https://www.chezmoi.io/)

My dotfiles are managed with [chezmoi](https://www.chezmoi.io/).

You can check them [online](https://gitlab.com/stephane.tzvetkov/dotfiles)
or with [chezmoi](https://www.chezmoi.io/)
(after [installing it](https://www.chezmoi.io/install/)).

E.g. pull this dotfile repository
and see what would change against your local configuration,
without actually applying any changes:

```console
$ chezmoi init https://gitlab.com/stephane.tzvetkov/dotfiles.git
$ chezmoi git pull -- --autostash --rebase && chezmoi diff
```


---
## Full installation from scratch

* Prerequisites:
    * [install Arch](https://wiki.archlinux.org/title/Installation_guide)
    * [install chezmoi](https://www.chezmoi.io/install/)
    * [install yay](https://github.com/Jguer/yay#installation)

* Create a pacman [hook](https://wiki.archlinux.org/title/Pacman#Hooks)
    in order to maintain an up to date package list:

    ```console
    # vi /usr/share/libalpm/hooks/<YOUR-HOSTNAME>_packages.hook
      + > [Trigger]
      + > Operation = Install
      + > Operation = Remove
      + > Type = Package
      + > Target = *
      + > 
      + > [Action]
      + > When = PostTransaction
      + > Exec = /bin/sh -c '/usr/bin/pacman -Qqe > /home/<YOUR-USERNAME>/.config/<YOUR-HOSTNAME>_packages.txt && chown <YOUR-USERNAME>:<YOUR-USERNAME> /home/<YOUR-USERNAME>/.config/<YOUR-HOSTNAME>_packages.txt'
    ```

* Initialize and apply my dotfiles with chezmoi:

    ```console
    $ chezmoi init --apply git@gitlab.com:stephane.tzvetkov/dotfiles.git
    ```

* Install all my framework-laptop packages:

    ```console
    $ sudo yay -S --needed - < ~/.config/framework-laptop_packages.txt
    ```

* See <https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#List_of_installed_packages>
    for more details about maintaining an up to date package list.


* Setup lemurs (the TUI display/login manager I use):

    ```console
    $ sudo mkdir -p /etc/lemurs/wayland/
    $ sudo vi /etc/lemurs/wayland/swayfx
      + > #! /bin/sh
      + > exec sway
    $ sudo systemctl enable lemurs
    ```

* Setup `pipewire`, `pipewire-pulse` and `wireplumber`:

    ```console
    $ systemctl --user enable pipewire
    $ systemctl --user enable pipewire-pulse
    $ systemctl --user enable wireplumber
    
    $ systemctl --user restart pipewire
    $ systemctl --user restart pipewire-pulse
    $ systemctl --user restart wireplumber
    ``
